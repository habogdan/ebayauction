$(function() {
    /*  Submit form using Ajax */
    $('button[id=login][type=submit]').click(function(e) {

        //Prevent default submission of form
        e.preventDefault();

        var data = {
            "username": $('#username_login').val(),
            "password":$('#password_login').val()
        };

        $.post({
            url : 'loginProcess',
            data : JSON.stringify(data),
            contentType: "application/json",
            success : function(res) {
                console.log(res);

                if(res.success === "true"){
                    //Set response
                    window.location.href = "/menu";


                }else{
                    //Set error messages
                    alert("Username or password is incorrect");

                }
            }
        })
    });

    //Register Ajax
    $('button[id=register][type=submit]').click(function(e) {

        //Prevent default submission of form
        e.preventDefault();

        var data = {
            "username": $('#username').val(),
            "password":$('#password').val(),
            "firstname":$('#firstname').val(),
            "lastname":$('#surname').val(),
            "email":$('#email').val()
        };

        $.post({
            url : 'registerProcess',
            data : JSON.stringify(data),
            contentType: "application/json",
            success : function(res) {
                console.log(res);

                if(res.success === "true"){
                    //Set response
                    //window.location.href = "/menu";
                    alert("Registration successful, you may now login")

                }else{
                    //Set error messages
                    alert("User already exists");

                }
            }
        })
    });
});