<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bogdan
  Date: 3/20/2019
  Time: 5:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FirstPage</title>
</head>
<body>

<div>
    <form:form id="regForm" modelAttribute="user" action="registerProcess" method="post">
        <table align="center">
            <tr>
                <td>
                    <form:label path="username">Username</form:label>
                </td>
                <td>
                    <form:input path="username" name="username" id="username" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="password">Password</form:label>
                </td>
                <td>
                    <form:password path="password" name="password" id="password" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="firstname">First Name</form:label>
                </td>
                <td>
                    <form:input path="firstname" name="firstname" id="firstname" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="lastname">Last Name</form:label>
                </td>
                <td>
                    <form:input path="lastname" name="surname" id="surname" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="email">Email</form:label>
                </td>
                <td>
                    <form:input path="email" name="email" id="email" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <form:button id="register" name="register">Register</form:button>
                </td>
            </tr>
        </table>
    </form:form>
</div>

<div>
    <form:form id="loginForm" modelAttribute="login" action="loginProcess" method="post">
        <table align="center">
            <tr>
                <td>
                    <form:label path="username">Username: </form:label>
                </td>
                <td>
                    <form:input path="username" name="username" id="username_login" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="password">Password:</form:label>
                </td>
                <td>
                    <form:password path="password" name="password" id="password_login" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="left">
                    <form:button id="login" name="login">Login</form:button>
                </td>
            </tr>


        </table>

    </form:form>

</div>

</body>

<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<!--
<script src="${pageContext.request.contextPath}/resources/js/login.js"></script>
-->
<script type="text/javascript">
    $(function() {
        /*  Submit form using Ajax */
        $('button[id=login][type=submit]').click(function(e) {

            //Prevent default submission of form
            e.preventDefault();

            var data = {
                "username": $('#username_login').val(),
                "password":$('#password_login').val()
            };

            $.post({
                url : 'loginProcess',
                data : JSON.stringify(data),
                contentType: "application/json",
                success : function(res) {
                    res = JSON.parse(res);

                    if(res.success === "true"){
                        //Set response
                        window.location.href = "/welcome";
                        alert("Wellcome");

                    }else{
                        //Set error messages
                        alert("Username or password is incorrect");

                    }
                }
            })
        });

        //Register Ajax
        $('button[id=register][type=submit]').click(function(e) {

            //Prevent default submission of form
            e.preventDefault();

            var data = {
                "username": $('#username').val(),
                "password":$('#password').val(),
                "firstname":$('#firstname').val(),
                "lastname":$('#surname').val(),
                "email":$('#email').val()
            };

            $.post({
                url : 'registerProcess',
                data : JSON.stringify(data),
                contentType: "application/json",
                success : function(res) {
                    res = JSON.parse(res);

                    if(res.success === "true"){
                        //Set response
                        window.location.href = "/welcome";
                        alert("Registration successful, you may now login")

                    }else{
                        //Set error messages
                        let errors = "";
                        let errorsArray = JSON.parse(res.errors);
                        for(let i = 0; i < errorsArray.length; i++) {
                            errors = errors + errorsArray[i] + "\n";
                    }
                        alert(errors);

                    }
                }
            })
        });
    });

</script>

</html>
