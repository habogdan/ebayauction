package ro.devis.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ro.devis.model.Login;
import ro.devis.model.User;
import ro.devis.service.UserService;
import ro.devis.utils.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static ro.devis.utils.RSAKeyPairGenerator.generateRSAKeys;
import static ro.devis.utils.RSAUtil.encrypt;


@RequestMapping("")
@Controller
@SessionAttributes("uid")
public class LoginController {

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("user", new User());
        mav.addObject("login", new Login());
        return mav;
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logout(WebRequest request, SessionStatus status) {
        status.setComplete();
        request.removeAttribute("uid", WebRequest.SCOPE_SESSION);
        return "redirect:/login";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/loginProcess", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    public String performLogin(@RequestBody String json, HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException {
        JSONObject jsonObj = new JSONObject(json);
        Login login = new Login();
        login.setUsername(jsonObj.getString("username"));
        login.setPassword(jsonObj.getString("password"));
        HashMap<String, String> result = new HashMap<>();
        try {
            login.setPassword(Security.crypt(login.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        User user = userService.validateUser(login);
        if (null != user) {
            result.put("success", "true");
            request.getSession().setAttribute("uid", user.getId());
        } else {
            result.put("success", "false");
        }
        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/registerProcess", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    public String  addUser(@RequestBody String json, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JSONObject jsonObj = new JSONObject(json);
        User user = new User();
        user.setUsername(jsonObj.getString("username"));
        user.setPassword(jsonObj.getString("password"));
        generateRSAKeys(user.getUsername());
        user.setFirstname(encrypt((jsonObj.getString("firstname")),jsonObj.getString("username")));
        user.setLastname(encrypt((jsonObj.getString("lastname")),jsonObj.getString("username")));
        user.setEmail(jsonObj.getString("email"));
        HashMap<String, String> result = new HashMap<>();

        List<String> errors = new ArrayList<>();

        if(user.hasNull() || !user.isValidEmailAddress()) {
            errors.add("Emaild invalid");
        }
        else{
            try {
                user.setPassword(Security.crypt(user.getPassword()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            boolean success =  userService.register(user);
            if(!success) {
                errors.add("User already exists");
            }
        }

        if(!errors.isEmpty()) {
            result.put("success", "false");
            result.put("errors", (new JSONArray(errors)).toString());
        } else {
            result.put("success", "true");
            request.getSession().setAttribute("uid", user.getId());
        }

        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

}
