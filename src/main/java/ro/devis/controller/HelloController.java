package ro.devis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ro.devis.model.User;
import ro.devis.service.UserService;
import ro.devis.utils.RSAUtil;
import ro.devis.utils.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping("")
@Controller
@SessionAttributes("uid")
public class HelloController {

    public HelloController(UserService userService) {
        this.userService = userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    @RequestMapping(value="/welcome", method = RequestMethod.GET)
    public String printHello(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        if(!Security.checkUserAcces(request))
            return "redirect:/login";

        User currentUser = userService.curentUser(request.getSession().getAttribute("uid"));
        model.addAttribute("firstname",
                RSAUtil.decrypt(currentUser.getFirstname(),currentUser.getUsername()));
        model.addAttribute("lastname",
                RSAUtil.decrypt(currentUser.getLastname(),currentUser.getUsername()));

        return "hello";
    }
}
