package ro.devis.service;

import ro.devis.model.Login;
import ro.devis.model.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UserService {
    public List<User> listUsers();

    boolean register(User user);

    User validateUser(Login login) throws NoSuchAlgorithmException;

    User curentUser(Object id);
}
