package ro.devis.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.devis.dao.UserDAO;
import ro.devis.model.Login;
import ro.devis.model.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Component
@Service
@Transactional
public class UserServiceImpl implements UserService{

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    private UserDAO userDAO;

    @Override
    @Transactional
    public List<User> listUsers() {
        return this.userDAO.listUsers();
    }

    @Override
    public boolean register(User user) {
        return userDAO.register(user);

    }

    @Override
    public User validateUser(Login login) throws NoSuchAlgorithmException {
        return userDAO.validateUser(login);
    }

    public User curentUser (Object id){
        return userDAO.curentUser(id);
    }
}
