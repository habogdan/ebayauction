package ro.devis.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ro.devis.model.Login;
import ro.devis.model.User;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<User> listUsers() {
        Session session = this.sessionFactory.getCurrentSession();
        return (List<User>) session.createQuery("from User").list();
    }

//    @Autowired
//    DataSource datasource;
//    @Autowired
//    JdbcTemplate jdbcTemplate;

    public boolean register(User user) {

        Query queryUser = sessionFactory.getCurrentSession()
                .createQuery("select count(*) from User where username = :user or email = :email");
        queryUser.setParameter("user", user.getUsername());
        queryUser.setParameter("email", user.getEmail());
        Long count = (Long)queryUser.uniqueResult();

        if(count.longValue() != 0)
            return false;

        Query queryID = sessionFactory.getCurrentSession().createQuery("from User order by id_user DESC");
        queryID.setMaxResults(1);
        User last = (User) queryID.uniqueResult();
        if (last == null)
        {
            user.setId(1);
        }
        else{
            user.setId(last.getId()+1);
        }
        sessionFactory.getCurrentSession().save(user);


        return true;
    }

    public User validateUser(Login login) {

        String sql = "from User where username= :user and password= :pass";
        List result = sessionFactory.getCurrentSession()
                .createQuery(sql)
                .setParameter("user", login.getUsername())
                .setParameter("pass", login.getPassword()).list();



        return result.size() > 0 ? (User)result.get(0) : null;
    }

    public User curentUser(Object id){

        Query queryUser = sessionFactory.getCurrentSession()
                .createQuery("from User where id_user = :id_user");
        queryUser.setParameter("id_user", id);

        User user = (User)queryUser.uniqueResult();

        return user;
    }



}
