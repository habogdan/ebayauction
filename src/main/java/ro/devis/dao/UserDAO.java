package ro.devis.dao;

import ro.devis.model.Login;
import ro.devis.model.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UserDAO {

    public List<User> listUsers();

    boolean register(User user);

    User validateUser(Login login) throws NoSuchAlgorithmException;

    User curentUser (Object id);
}

