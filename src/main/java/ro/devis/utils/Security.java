package ro.devis.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Security {

    public static String crypt(String pass)  throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(pass.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }

    public static boolean checkUserAcces(HttpServletRequest request) {
        HttpSession sess = request.getSession();
        if(sess == null)
            return false;
        Object uid = sess.getAttribute("uid");
        if(uid == null || uid.equals(""))
            return false;

        return true;
    }

}
