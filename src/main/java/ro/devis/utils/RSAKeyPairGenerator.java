package ro.devis.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;

import static ro.devis.utils.Constants.PATH_RSA_KEYS;

public class RSAKeyPairGenerator {

    private PrivateKey privateKey;
    private PublicKey publicKey;

    public RSAKeyPairGenerator() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair pair = keyGen.generateKeyPair();
        this.privateKey = pair.getPrivate();
        this.publicKey = pair.getPublic();
    }

    public void writeToFile(String path, byte[] key) throws IOException {
        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key);
        fos.flush();
        fos.close();
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public static void generateRSAKeys(String username) throws NoSuchAlgorithmException, IOException {

        RSAKeyPairGenerator keyPairGenerator = new RSAKeyPairGenerator();

        String key_public = username + "_pub";
        String key_private = username + "_prv";

        keyPairGenerator.writeToFile(PATH_RSA_KEYS + key_public, keyPairGenerator.getPublicKey().getEncoded());
        keyPairGenerator.writeToFile(PATH_RSA_KEYS + key_private, keyPairGenerator.getPrivateKey().getEncoded());
    }
}
