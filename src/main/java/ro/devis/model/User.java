package ro.devis.model;

import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.*;

@Entity
@Table(name = "users")
@Component
public class User {

    @Id
    @Column(name = "ID_USER")
    private int id_user;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "EMAIL")
    private String email;

    @Lob
    @Column(name = "FIRST_NAME", columnDefinition = "BLOB")
    private byte[] firstname;

    @Lob
    @Column(name = "LAST_NAME", columnDefinition = "BLOB")
    private byte[] lastname;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {

        return id_user;
    }

    public void setId(int id) {

        this.id_user = id;
    }

    public boolean hasNull() {
        return username.equals("") || password.equals("") || firstname.equals("") || lastname.equals("") || email.equals("");
    }

    public boolean isValidEmailAddress() {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getFirstname() {
        return firstname;
    }

    public void setFirstname(byte[] firstname) {
        this.firstname = firstname;
    }

    public byte[] getLastname() {
        return lastname;
    }

    public void setLastname(byte[] lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "id_user=" + id_user + ", name=" + firstname;
    }

}
